# simple-logger-cornerjob #

A simple logger based on npm log2out.  Allows enabling/disabling logs.

## Install
```
#!bash
npm install --save simple-logger-cornerjob
```

## Logging
#
### Code
```
#!javascript
const logger = require('simple-logger-cornerjob').getLogger('Server');

logger.info('Hello world');
logger.error('Goodbye world');
```

### Output
```
#!bash
[2017-04-10T08:56:28.779Z] [INFO] Server - Hello world
[2017-04-10T08:56:28.803Z] [ERROR] Server - Goodbye world
```
#
You have access to all of the standard log levels:

* logger.trace();
* logger.info();
* logger.debug();
* logger.warn();
* logger.error();
* logger.fatal();

## Configuration
#
You can enable/disable logs by setting the envar **DISABLE_LOGS**=true/false.

*(This will only affect logs printed with simple-logger.  Logs printed with console will still show.)*

## With logging disabled
#
### Code
```
#!javascript
const logger = require('simple-logger-cornerjob').getLogger('Server');

logger.info('Hello world');
console.log('Goodbye world');
```

### Output
```
#!bash
Goodbye world
```
