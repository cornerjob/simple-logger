'use strict';

const DummyLogger = require('./DummyLogger.js');
const log2out = require('log2out');

const LogProvider = {
  getLogger: (fileName) => {
    if (String(process.env.DISABLE_LOGS).toLowerCase() === 'true') {
      return new DummyLogger(fileName);
    }
    return log2out.getLogger(fileName);
  }
};

module.exports = LogProvider;
