'use strict';

const DummyLogger = function () {};

DummyLogger.prototype.trace = function () {};
DummyLogger.prototype.info = function () {};
DummyLogger.prototype.debug = function () {};
DummyLogger.prototype.warn = function () {};
DummyLogger.prototype.error = function () {};
DummyLogger.prototype.fatal = function () {};

module.exports = DummyLogger;
